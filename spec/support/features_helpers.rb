def click_to_home
  click_link('home')
end

def sign_up(user)
  fill_in("user[email]", with: "user@example.com")
  fill_in("user[password]", with: "foobarbaz")
  fill_in("user[password_confirmation]", with: "foobarbaz")
  fill_in("user[full_name]", with: "foobar")
  click_button  "Buat Akun"
end

def sign_in(user, options= {})
  # visit new_user_session_path
  # filling_sign_in_form user
  # visit admin_root_path if options[:admin] == true
  page.set_rack_session(user_id: user.id)
end

def filling_sign_in_form(user)
  fill_in("user[email]", with: user.email)
  fill_in("user[password]", with: user.password)
  click_button  "Masuk"
end

def sign_out
  first(:link, "Keluar").click
end

def login(user)
  post_via_redirect user_session_path, 'user[email]' => user.email, 'user[password]' => user.password
end
