def file_data(name)
  File.read(Rails.root.to_s + "/spec/fixtures/files/#{name}")
end
