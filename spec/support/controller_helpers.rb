module ControllerHelpers
  # def login_admin
  #   before(:each) do
  #     @request.env["devise.mapping"] = Devise.mappings[:admin]
  #     admin = FactoryGirl.create(:admin)
  #     sign_in user: admin # sign_in(scope, resource)
  #   end
  # end

  # def login_user
  #   before(:each) do
  #     @request.env["devise.mapping"] = Devise.mappings[:user]
  #     user = FactoryGirl.create(:user)
  #     # user.confirm! # or set a confirmed_at inside the factory. Only necessary if you are using the "confirmable" module
  #     sign_in user
  #   end
  # end

  def auth0_new_user
    {
      provider: 'auth0',
      uid: 'google-oauth2|this-is-the-google-id',
      info: {
        name: 'John Foo',
        email: 'johnfoo@example.org',
        nickname: 'john',
        first_name: 'John',
        last_name: 'Foo',
        location: 'en',
        image: 'https://example.org/john.jpg'
      },
      credentials: {
        token: 'XdDadllcas2134rdfdsI',
        expires: 'false',
        id_token: 'eyJhbGciOiJIUzI1NiIsImN0eSI6IkpXVCJ9.eyJuYW1lIjoiSm9obiBGb28ifQ.lxAiy1rqve8ZHQEQVehUlP1sommPHVJDhgPgFPnDosg',
        token_type: 'bearer',
      },
      extra: {
        raw_info: {
          email: 'johnfoo@example.org',
          email_verified: 'true',
          name: 'John Foo',
          given_name: 'John',
          family_name: 'Foo',
          picture: 'https://example.org/john.jpg',
          gender: 'male',
          locale: 'en',
          clientID: 'nUBkskdaYdsaxK2n9',
          user_id: 'google-oauth2|this-is-the-google-id',
          nickname: 'john',
          identities: [{
            access_token: 'this-is-the-google-access-token',
            provider: 'google-oauth2',
            expires_in: '3599',
            user_id: 'this-is-the-google-id',
            connection: 'google-oauth2',
            isSocial: 'true',
          }],
          user_metadata: {
            name: 'John Foo'
          },
          created_at: '2014-07-15T17:19:50.387Z'
        }
      }
    }
  end

  def auth0_registered_user(email:)
    {
      provider: 'auth0',
      uid: 'google-oauth2|this-is-the-google-id',
      info: {
        name: 'John Foo',
        email: email,
        nickname: 'john',
        first_name: 'John',
        last_name: 'Foo',
        location: 'en',
        image: 'https://example.org/john.jpg'
      },
      credentials: {
        token: 'XdDadllcas2134rdfdsI',
        expires: 'false',
        id_token: 'eyJhbGciOiJIUzI1NiIsImN0eSI6IkpXVCJ9.eyJuYW1lIjoiSm9obiBGb28ifQ.lxAiy1rqve8ZHQEQVehUlP1sommPHVJDhgPgFPnDosg',
        token_type: 'bearer',
      },
      extra: {
        raw_info: {
          email: email,
          email_verified: 'true',
          name: 'John Foo',
          given_name: 'John',
          family_name: 'Foo',
          picture: 'https://example.org/john.jpg',
          gender: 'male',
          locale: 'en',
          clientID: 'nUBkskdaYdsaxK2n9',
          user_id: 'google-oauth2|this-is-the-google-id',
          nickname: 'john',
          identities: [{
            access_token: 'this-is-the-google-access-token',
            provider: 'google-oauth2',
            expires_in: '3599',
            user_id: 'this-is-the-google-id',
            connection: 'google-oauth2',
            isSocial: 'true',
          }],
          user_metadata: {
            name: 'John Foo'
          },
          created_at: '2014-07-15T17:19:50.387Z'
        }
      }
    }
  end
end
