# module OmniauthMacros
  def mock_auth_hash_facebook
    # The mock_auth configuration allows you to set per-provider (or default)
    # authentication hashes to return during integration testing.
    OmniAuth.config.mock_auth[:facebook] = OmniAuth::AuthHash.new({
      'provider' => 'facebook',
      'uid' => '123545',
      'info' => {
        'email' => 'facebooker@example.com',
        'image' => '//res.cloudinary.com/instilla/image/upload/s--1FFxUoOX--/v1474298240/avatar/default_avatar.png',
        'first_name' => 'facebook',
        'last_name' => 'user',
        'location' => 'jakarta',
        'urls' => {
          'Facebook' => 'facebook.com/facebooker'
        }
      },
      'credentials' => {
        'token' => 'mock_token',
        'secret' => 'mock_secret'
      },
      'extra' => {
        'raw_info' => {
          'id' =>'1234567',
          'name' =>'Joe Bloggs',
          'first_name' =>'facebook',
          'last_name' =>'user',
          'link' =>'http://www.facebook.com/jbloggs',
          'username' =>'jbloggs',
          'location' => { 'id' =>'123456789', 'name' =>'Palo Alto, California' },
          'gender' =>'male',
          'email' =>'joe@bloggs.com',
          'timezone' => -8,
          'locale' =>'en_US',
          'verified' =>true,
          'updated_time' =>'2011-11-11T06:21:03+0000'
        }
      }
    })
  end

  def mock_auth_hash_google
    # The mock_auth configuration allows you to set per-provider (or default)
    # authentication hashes to return during integration testing.
    OmniAuth.config.mock_auth[:google_oauth2] = OmniAuth::AuthHash.new({
      'provider' => 'google_oauth2',
      'uid' => '123545',
      'info' => {
        'email' => 'googler@example.com',
        'first_name' => 'google',
        'last_name' => 'user',
        'name' => 'google user'
      },
      'credentials' => {
        'token' => 'mock_token',
        'secret' => 'mock_secret'
      },
      'extra' => {
        'raw_info' => {
            'sub' => "123456789",
            'email' => "user@domain.example.com",
            'email_verified' => true,
            'name' => "John Doe",
            'given_name' => "John",
            'family_name' => "Doe",
            'profile' => "https://plus.google.com/123456789",
            'picture' => "https://lh3.googleusercontent.com/url/photo.jpg",
            'gender' => "male",
            'birthday' => "0000-06-25",
            'locale' => "en",
            'hd' => "company_name.com"
        }
      }
    })
  end

  def mock_auth_hash_link
    # The mock_auth configuration allows you to set per-provider (or default)
    # authentication hashes to return during integration testing.
    OmniAuth.config.mock_auth[:linkedin] = OmniAuth::AuthHash.new({
      'provider' => 'linkedin',
      'uid' => '123545',
      'info' => {
        'email' => 'link@example.com',
        'image' => '//res.cloudinary.com/instilla/image/upload/s--1FFxUoOX--/v1474298240/avatar/default_avatar.png',
        'first_name' => 'link',
        'last_name' => 'user',
        'location' => 'jakarta',
        'urls' => {
          'public_profile' => 'linkedin.com/link'
        }
      },
      'credentials' => {
        'token' => 'mock_token',
        'secret' => 'mock_secret'
      }
    })
  end
# end
