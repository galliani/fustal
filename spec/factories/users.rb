FactoryGirl.define do
  factory :user, aliases: [:email_confirmed_user, :last_user], class: ::User do
    sequence(:email) { |n| "Person_#{n}@example.com" }
    password "foobarbaz"
    password_confirmation "foobarbaz"
    sequence(:name) { |n| "Person #{n}" }
    sequence(:phone_number) { |n| "05392421#{n}" }
    understanding "true"
    avatar_source 'auth0'
    avatar 'https://s.gravatar.com/avatar/875c88187ba24d1db6aa534ecad8ff7e?s=480&r=pg&d=https%3A%2F%2Fcdn.auth0.com%2Favatars%2Fus.png'

    factory :admin do
      admin true
      after(:create) do |user|
      end
	  end

    trait :with_full_profile do
      phone_number "081398979879"
      about "Lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor lorem ipsum dolor"
      last_education "D3/Sarjana"
      marital_status "Menikah"
      after(:create) do |user, _|
      end
    end

    trait :admin do
      admin true
      after(:create) do |user, _|
      end
    end

    trait :with_user_details do
      after(:create) do |user, _|
      end
    end

    trait :approved do
      after(:create) do |user, _|
      end
    end

    trait :blocked do
      after(:create) do |user, _|
      end
    end
  end
end
