# require 'rails_helper'
#
# RSpec.describe Auth0Controller, type: :controller do
#   describe 'POST #callback' do
#     let(:user) { users(:jdoe_1) }
#
#     context 'with valid params of a new user' do
#       before do
#         request.env['omniauth.auth'] = auth0_new_user
#       end
#
#       it 'creates a new user' do
#         expect { get :callback }.to change(User, :count).by(1)
#       end
#     end
#
#     # context 'with valid params of a registered user' do
#     #   before do
#     #     request.env['omniauth.auth'] = auth0_registered_user(email: user.email)
#     #   end
#     #
#     #   it 'does not create a new user' do
#     #     expect { get :callback }.to change(User, :count).by(0)
#     #   end
#     # end
#   end
# end
