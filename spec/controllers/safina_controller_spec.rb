require 'rails_helper'
require 'support/json_helpers'

RSpec.describe SafinaController, type: :controller do
  describe 'GET #landing' do
    it 'should render successfully' do
      get :landing

      expect(response.status).to eq 200
      expect(assigns(:title)).to match App::Title::CARD
    end
  end

  describe 'GET #help' do
    it 'should render successfully' do
      get :help

      expect(response.status).to eq 200
      expect(assigns(:title)).to match 'Bantuan |' + App::Title::CARD
    end
  end

  describe 'GET #sharia_compliance' do
    it 'should render successfully' do
      get :sharia_compliance

      expect(response.status).to eq 200
    end
  end

  describe 'GET #order_form' do
    context 'plain' do
      it 'should render successfully' do
        get :order_form

        expect(response.status).to eq 200
        expect(assigns(:title)).to match 'Formulir Pemesanan |' + App::Title::CARD
      end
    end

    context 'after submitting the price simulation' do
      before(:each) do
        url = ENV['CURRENCYLAYER_API_URL'] + "?access_key=#{ENV['CURRENCYLAYER_API_KEY']}&currencies=IDR"

        @stub = stub_request(:get, url).to_return(
          status: 200, body: '{"quotes": { "USDIDR": 13400 } }', headers: {}
        )
      end

      it 'should render successfully' do
        get :order_form, usd_amount: 100

        expect(response.status).to eq 200
        expect(assigns(:title)).to match 'Formulir Pemesanan |' + App::Title::CARD
        expect(@stub).to have_been_requested
      end
    end
  end

  describe 'GET #view_card' do
    context 'not yet authenticated' do
      it 'should redirect when no card is passed as the params' do
        get :view_card

        expect(response.status).to eq 302
        expect(response.body).to match '404'
      end

      it 'should show the page with the card locked when passed valid card in params' do
        card = 'recP8ZIw7LS39caqs'
        card_response = file_data('valid_card_retrieval.json')

        stub = stub_request(
          :get, ENV['AIRTABLE_API_URL'] + ENV['AIRTABLE_SAFINA'] + "PrepaidCard/#{card}"
        ).to_return(:status => 200, :body => card_response.as_json, :headers => {'Authorization'=>'Bearer keydQbBOM9VSsBdJT'})

        get :view_card, {card: card}

        expect(stub).to have_been_requested
        expect(response.status).to eq 200
      end

      it 'should redirect to 404 when passed invalid card in params' do
        card = 'a'

        stub = stub_request(
          :get, ENV['AIRTABLE_API_URL'] + ENV['AIRTABLE_SAFINA'] + "PrepaidCard/#{card}"
        ).to_return(:status => 404, :body => '{}', :headers => {'Authorization'=>'Bearer keydQbBOM9VSsBdJT'})

        get :view_card, {card: card}

        expect(stub).to have_been_requested
        expect(response.status).to eq 302
        expect(response.body).to match '404'        
      end      
    end

    context 'after successfully authenticated' do
    end
  end

  describe 'POST #open_card' do
    before(:each) do
      @card = 'recP8ZIw7LS39caqs'
      
      setup_simple_crypt_for_card  
    end

    context 'successful authentication' do
      it 'should redirect to the view card page with extra params to open the card' do
        pass = '12410081399279500'

        post :open_card, {card: @card, password: pass, cardKey: @crypt.encrypt_and_sign(pass)}

        expect(response.status).to eq 200
      end      
    end

    context 'failed authentication' do
      it 'should redirect to the view card page' do
        post :open_card, {card: @card, password: 'lorem', cardKey: @crypt.encrypt_and_sign('wrongpassword')}

        expect(response.status).to eq 422
      end      
    end    
  end

  describe 'POST #request_reload_card' do
    before(:each) do
      @stub = stub_request(
        :post, ENV['AIRTABLE_API_URL'] + ENV['AIRTABLE_SAFINA'] + "ReloadForm"
      ).to_return(:status => 201, :body => '{}', :headers => {'Authorization'=>'Bearer keydQbBOM9VSsBdJT'})      
    end

    context 'valid reload request' do
      it 'should return 200 status' do
        post :request_reload_card, {card: 'recP8ZIw7LS39caqs', amount: '50', format: :js}

        expect(@stub).to have_been_requested
        expect(response.status).to eq 200
        expect(flash[:notice]).to be_present
      end      
    end

    context 'invalid reload request' do
      it 'should return 200 status' do
        post :request_reload_card, {card: 'recP8ZIw7LS39caqs', amount: '10', format: :js}

        expect(@stub).to_not have_been_requested
        expect(flash[:danger]).to be_present
      end      
    end    
  end

  def setup_simple_crypt_for_card
    key = ActiveSupport::KeyGenerator.new('card_password').generate_key(ENV['CARD_SALT'])
    @crypt = ActiveSupport::MessageEncryptor.new(key)
  end  
end
