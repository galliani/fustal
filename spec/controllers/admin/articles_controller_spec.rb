require 'rails_helper'

RSpec.describe Admin::ArticlesController, type: :controller do
  fixtures('articles')
  let(:article) { articles(:article_1) }

  describe 'GET index' do
    it 'assigns all objects as @objects' do
      get :index

      expect(response.status).to eq 302
    end
  end

  describe 'POST #bulk_sync' do
    context 'valid request' do
      it 'redirects as a response' do
        post :bulk_sync

        expect(response.status).to eq 302
      end
    end
  end
end
