require 'rails_helper'

RSpec.describe Admin::SafinaController, type: :controller do
  describe 'GET #prepare_invoice' do
    it 'should render Successfully' do
      get :prepare_invoice

      expect(response.status).to eq 302
    end
  end

  describe 'POST #create_invoice' do
    before :each do
      @stub = stub_request(
        :post, ENV['DUITKU_API_URL'] + 'inquiry'
      ).to_return(:status => 201, :body => "", :headers => {})
    end

    context 'valid request' do
      it 'redirects as a response' do
        post :create_invoice, {
          merchantCode: 'D0125',
          paymentAmount: '250000',
          merchantOrderId: 'recIr20UMDtE3MdCz',
          productDetails: 'Safina Card #97260B84 untuk Fadhillah Indrabudi',
          merchantUserInfo: 'galih0muhammad@gmail.com'
        }

        expect(response.status).to eq 302
      end
    end
  end
end
