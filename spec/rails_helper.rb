# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require 'spec_helper'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'capybara/rails'
require 'capybara/rspec'

require 'active_support/testing/time_helpers'
require 'sucker_punch/testing/inline'
require 'rack_session_access/capybara'

# To mock http request
require 'webmock/rspec'
WebMock.disable_net_connect!(allow_localhost: true)

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :rspec
    with.library :rails
  end
end

# Add additional requires below this line. Rails is not loaded until this point!

Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

# Checks for pending migrations before tests are run.
# If you are not using ActiveRecord, you can remove this line.
ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|
  config.include Capybara::DSL
  config.include ActiveSupport::Testing::TimeHelpers
  config.include Rails.application.routes.url_helpers

  # Include to allow stub auth in feature specs
  # config.include Warden::Test::Helpers
  config.after :each do
    Warden.test_reset!
  end

  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.global_fixtures = :all

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  # config.use_transactional_fixtures = false
  # if you want to use fixture
  config.use_transactional_fixtures = true

  config.include ControllerHelpers, :type => :controller
  config.infer_spec_type_from_file_location!

  # run retry only on features
  config.around :each, :js do |ex|
    ex.run_with_retry retry: 2
  end
end
