require 'rails_helper'
require 'faker'

describe 'Article' do
  subject { Article.new }

  it { is_expected.to respond_to(:contentful_id) }
  it { is_expected.to respond_to(:slug) }
  it { is_expected.to respond_to(:title) }
  it { is_expected.to respond_to(:teaser) }
  it { is_expected.to respond_to(:header_url) }
  it { is_expected.to respond_to(:body) }
  it { is_expected.to respond_to(:published_at) }
  it { is_expected.to respond_to(:author_details) }
  it { is_expected.to respond_to(:author_id) }
  it { is_expected.to respond_to(:author_email) }
  it { is_expected.to respond_to(:author_name) }
end
