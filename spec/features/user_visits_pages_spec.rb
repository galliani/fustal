require 'rails_helper'

feature 'UserVisitsPages', type: :feature do
  # PUSTAKA
  scenario 'go to the pustaka root page' do
    visit root_path

    expect(page).to have_title('Pustaka | Pembahasan ilmu muamalah | Kapiten')
  end

  scenario 'go to pustaka article page' do
    article = Article.first

    visit article_path(article.id)

    expect(page).to have_title("#{article.title} | Kapiten")
    expect(page).to have_content(article.title)
  end

  scenario 'go to about us page' do
    visit page_path(id: 'about')

    expect(page).to have_title('Tentang Kami | Kapiten')
  end

  # SAFINA
  scenario 'go to safina landing page' do
    visit safina_root_path

    expect(page).to have_title('Safina | Kartu Debit Visa Untuk Belanja Online di Situs | Kapiten')
  end

  scenario 'go to safina help page' do
    visit safina_knowledge_base_path

    expect(page).to have_title('Safina | Kartu Debit Visa Untuk Belanja Online di Situs | Kapiten')
  end

  scenario 'go to safina sharia compliance page' do
    visit safina_sharia_compliance_path

    expect(page).to have_title('Safina | Kartu Debit Visa Untuk Belanja Online di Situs | Kapiten')
  end  

  scenario 'go to safina contact page' do
    visit page_path(id: 'contact')

    expect(page).to have_title('Hubungi Kami | Kapiten')
  end

  scenario 'go to safina order page' do
    visit safina_order_form_path

    expect(page).to have_title('Safina | Kartu Debit Visa Untuk Belanja Online di Situs | Kapiten')
  end  
end