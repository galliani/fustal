class AddKeywordsToArticlesTable < ActiveRecord::Migration
  def self.up
  	add_column :articles, :keywords, :text, array: true, default: '{}'
  	add_index  :articles, :keywords, using: 'gin'
  end

  def self.down
  	remove_index  :articles, :keywords
  	remove_column :articles, :keywords
  end  
end
