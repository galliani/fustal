class CreateUserStructure < ActiveRecord::Migration
  def change
    unless table_exists?(:users)
      create_table(:users) do |t|
        ## Database authenticatable
        t.string :email,              null: false, default: ''
        t.string :encrypted_password, null: false, default: ''

        ## Admin
        t.boolean :admin, null: false, default: false

        ## Lock
        t.boolean :locked, null: false, default: false

        ## Recoverable
        t.string   :reset_password_token
        t.datetime :reset_password_sent_at

        ## Rememberable
        t.datetime :remember_created_at

        ## Trackable
        t.integer  :sign_in_count, default: 0, null: false
        t.datetime :current_sign_in_at
        t.datetime :last_sign_in_at
        t.string   :current_sign_in_ip
        t.string   :last_sign_in_ip

        ## Confirmable
        t.string   :confirmation_token
        t.datetime :confirmed_at
        t.datetime :confirmation_sent_at
        t.string   :unconfirmed_email # Only if using reconfirmable

        ## Lockable
        # t.integer  :failed_attempts, :default => 0, :null => false # Only if lock strategy is :failed_attempts
        # t.string   :unlock_token # Only if unlock strategy is :email or :both
        # t.datetime :locked_at

        t.timestamps null: false
      end

      add_index :users, :email,                unique: true
      add_index :users, :reset_password_token, unique: true
      add_index :users, :confirmation_token,   unique: true
      # add_index :users, :unlock_token,         :unique => true

      add_column :users, :name, :string, null: false
      add_column :users, :deleted_at, :datetime, index: true
      add_column :users, :avatar, :string
      add_column :users, :profile, :jsonb, default: {}
      add_column :users, :preferences, :jsonb, default: {}
      add_column :users, :auth_token, :string, null: false, default: ''
      add_column :users, :slug, :string, index: true, unique: true
      add_index  :users, :profile, using: :gin
      add_index  :users, :auth_token, unique: true
    end

    unless table_exists?(:identities)
      create_table :identities do |t|
        t.references :user, index: true
        t.string :provider, null: false
        t.string :uid, null: false
        t.string :token, null: false
        t.string :secret
        t.datetime :expires_at
        t.string  :email
        t.string  :image
        t.string  :nickname
        t.string  :first_name
        t.string  :last_name
        t.string  :location
        t.string  :public_url

        t.timestamps null: false
      end
      add_foreign_key :identities, :users
      add_index :identities, [:user_id, :provider, :uid]
      add_index :identities, [:provider, :uid], unique: true
    end
  end
end
