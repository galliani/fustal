class ExtendUserProfile < ActiveRecord::Migration
  def self.up
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :username, :string
    add_column :users, :auth_with, :string
    add_column :users, :avatar_source, :string, default: ''
    remove_column :users, :auth_token
  end

  def self.down
    remove_column :users, :first_name
    remove_column :users, :last_name
    remove_column :users, :username
    remove_column :users, :auth_with
    remove_column :users, :avatar_source
    add_column :users, :auth_token, :string
  end
end
