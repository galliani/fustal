class CreateArticles < ActiveRecord::Migration
  def change
    create_table  :articles do |t|
      t.string    :contentful_id, null: false
      t.string    :slug, limit: 191, index: true
      t.string    :title, null: false
      t.string    :category
      t.string    :teaser
      t.text      :body
      t.datetime  :published_at
      t.datetime  :last_sync_at
      t.jsonb     :header_details, default: {}
      t.jsonb     :author_details, default: {}

      t.timestamps null: false
    end
  end
end
