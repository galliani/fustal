class CreateBasicStructure < ActiveRecord::Migration
  def change
    # Use Friendly ID https://github.com/norman/friendly_id
    unless table_exists?(:friendly_id_slugs)
      create_table :friendly_id_slugs do |t|
        t.string   :slug,           null: false
        t.integer  :sluggable_id,   null: false
        t.string   :sluggable_type, limit: 50
        t.string   :scope
        t.datetime :created_at
      end
      add_index :friendly_id_slugs, :sluggable_id
      add_index :friendly_id_slugs, [:slug, :sluggable_type]
      add_index :friendly_id_slugs, [:slug, :sluggable_type, :scope], unique: true
      add_index :friendly_id_slugs, :sluggable_type
    end

    unless table_exists?(:activities)
      # Use Public Activity https://github.com/chaps-io/public_activity
      create_table :activities do |t|
        t.belongs_to :trackable, polymorphic: true
        t.belongs_to :owner, polymorphic: true
        t.string  :key
        t.text    :parameters
        t.belongs_to :recipient, polymorphic: true

        t.timestamps null: false
      end

      add_index :activities, [:trackable_id, :trackable_type]
      add_index :activities, [:owner_id, :owner_type]
      add_index :activities, [:recipient_id, :recipient_type]
    end

    unless table_exists?(:attachinary_files)
      # Use Attachinary https://github.com/assembler/attachinary
      create_table :attachinary_files do |t|
        t.references :attachinariable, polymorphic: true
        t.string :scope

        t.string :public_id
        t.string :version
        t.integer :width
        t.integer :height
        t.string :format
        t.string :resource_type
        t.timestamps null: false
      end
      add_index :attachinary_files, [:attachinariable_type, :attachinariable_id, :scope], name: 'by_scoped_parent'
    end

    drop_table :posts if table_exists?(:posts)
    drop_table :topics if table_exists?(:topics)
    drop_table :boards if table_exists?(:boards)
    drop_table :custom_auto_increments if table_exists?(:custom_auto_increments)
  end
end
