class ApplicationController < ActionController::Base
  include CurrentUser
  # Prevent CSRF attacks by raising an exception.
  protect_from_forgery with: :exception
  before_action :detect_device_format, unless: Proc.new { |c| c.request.format.json? }
  before_filter :reject_locked!, if: :devise_controller?
  before_filter :set_locale

  helper_method :current_user?, :require_admin!

  # Routing
  def send_contact_email
    @name = params[:name]
    @email = params[:email]
    @subject = params[:subject]
    @message = params[:message]
    
    if @name.blank?
      flash[:alert] = "Tolong isi namamu sebelum mengirimkan pesan. Terima kasih."
      redirect_to page_path(id: 'contact')
    elsif @email.blank? || @email.scan(/\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i).size < 1
      flash[:alert] = "Kamu harus menuliskan alamat email yang betul dan valid. Terima kasih."
      redirect_to page_path(id: 'contact')
    elsif @message.blank? || @message.length < 10
      flash[:alert] = "Isi pesanmu kosong. Sedikitnya, pesanmu harus mempunyai 10 karakter."
      redirect_to page_path(id: 'contact')
    elsif @message.scan(/<a href=/).size > 0 || @message.scan(/\[url=/).size > 0 || @message.scan(/\[link=/).size > 0 || @message.scan(/http:\/\//).size > 0
      flash[:alert] = "Kamu tidak bisa mengirim tautan atau link website. Mohon pengertiannya."
      redirect_to page_path(id: 'contact')
    else
      ContactMailer.contact_message(@name, @email, @subject, @message).deliver_later
      
      redirect_to safina_root_path, notice: "Pesanmu telah dikirim. Terima kasih."
    end
  end

  # Only permits admin users
  def require_admin!
    if current_user && !current_user.admin?
      redirect_to user_root_path
    end
  end

  # This two methods insert instance variables to affect the layout
  def set_as_static
    @static = true
  end
  def admin_layout
    @admin_layout = true
  end

  private

  def detect_device_format
    case request.user_agent
    when /iPad/i
      request.variant = :tablet
    when /iPhone/i
      request.variant = :phone
    when /Android/i && /mobile/i
      request.variant = :phone
    when /Android/i
      request.variant = :tablet
    when /Windows Phone/i
      request.variant = :phone
    else
      request.variant = :desktop
    end
  end

  def set_locale
    # I18n.locale = params[:locale] if params[:locale].present?
    # I18n.locale = current_user.locale # alternative 1
    # if cookies[:user_locale] && I18n.available_locales.include?(cookies[:user_locale].to_sym)
    #   l = cookies[:user_locale].to_sym
    # else
    #   l = I18n.default_locale
    #   cookies.permanent[:user_locale] = l
    # end
    I18n.locale = I18n.default_locale
  end
end
