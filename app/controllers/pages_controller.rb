class PagesController < ApplicationController
  layout 'application'
  include HighVoltage::StaticPage
  before_filter :set_as_static
  before_filter :set_instance_variable

  # link with /pages in landing
  private

  def set_instance_variable
    case params[:id]
    when 'safar'
      @keywords = 'Beli tiket pesawat, mencicil tiket pesawat, beli tiket dengan cicilan'
    when 'upgrade'
      @browser = browser.name
    end
  end
end
