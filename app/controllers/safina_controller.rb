class SafinaController < ApplicationController
  before_filter :set_meta_keywords, except: [
    :view_card, :open_card
  ]
  before_filter :prepare_card, only: :view_card
  skip_before_filter :verify_authenticity_token, only: :request_reload_card


  def landing
  	@title = App::Title::CARD
  end

  def help
  	@title = 'Bantuan |' + App::Title::CARD
  end

  def sharia_compliance
  end

  def order_form
    require 'bigdecimal'

  	@title = 'Formulir Pemesanan |' + App::Title::CARD
    @usd_amount = params[:usd_amount]

    if @usd_amount.present?
      uri = URI.parse(ENV['CURRENCYLAYER_API_URL'] + "?access_key=#{ENV['CURRENCYLAYER_API_KEY']}&currencies=IDR")
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Get.new(uri.request_uri)

      response = JSON.parse http.request(request).body.as_json
      rate = BigDecimal.new(response['quotes']['USDIDR'])
      @idr_amount = BigDecimal.new(@usd_amount.to_i + 5) * rate
    end
  end

  def view_card
    render layout: false
  end

  def open_card
    crypt = setup_simple_crypt

    if params[:password] == crypt.decrypt_and_verify(params[:cardKey]) 
      render status: :ok, json: { 'status' => 'success' }
    else
      render status: :unprocessable_entity, json: { 'status' => 'failed' }
    end
  end

  def request_reload_card
    @success = false

    if BigDecimal.new(params[:amount].gsub(',', '.')) >= BigDecimal.new('25.00')
      client = AirtableClient.new(extension: ENV['AIRTABLE_SAFINA'])

      form_fields = {
        :"PrepaidCard" => [params[:card]],
        :"Amount" => params[:amount],
        :"Status" => 'PENDING'
      }
      reload_input = { :fields => form_fields }.to_json

      res = client.send_post_request(endpoint: 'ReloadForm', data: reload_input)
      @success = true if res.success?
    end

    respond_to do |format|
      format.js do 
        if @success
          flash.now[:notice] = 'Terima kasih, kami akan mengontakmu via email'
        else
          flash.now[:danger] = 'Maaf, mohon coba lagi'
        end
      end
    end
  end

  def redirector
    # returned params by Duitku
    merchant_invoice_uid = params[:merchantOrderId]
    provider_invoice_uid = params[:reference]
    status = params[:resultCode]

    if status.to_s == '00'
      # Immediately send paying customer to the card page
      redirect_to safina_view_card_path(card: merchant_invoice_uid, xyz: reference)
    else #could be 01 or 02, which means pending/failed or cancelled
      redirect_to failed_payment_path
    end
  end

  private

  def set_meta_keywords
    @keywords = 'Kartu kredit syariah, kartu prabayar Visa, kartu virtual, belanja online'
  end

  def prepare_card(authenticated: false)
    begin
      raise ArgumentError if params['card'].nil?

      retrieve_card_attributes(card: params['card'])

      last_digits_number = @card_attributes['card_number'].split(' ').last
      @card_attributes['masked_card_number'] = '**** **** **** ' + last_digits_number
      @card_attributes['masked_expiry_date'] = '**/**'
      @card_attributes['masked_cardholder_name'] = @card_attributes['cardholder_name'].gsub(/[a-zA-z]/, '*')        
      @card_attributes['initial_balance'] = @card_attributes['initial_balance'].to_sentence

      @card = SafinaCard.new(@card_attributes)
      raise ArgumentError if @card.nil?

      gon.push({
        maskedCardholderName: @card.masked_cardholder_name,
        maskedCardNumber: @card.masked_card_number,
        maskedExpiryDate: @card.masked_expiry_date,
        cardNumber: @card.card_number,
        cardholderName: @card.cardholder_name,
        expiryDate: @card.expiry_date
      })

      if params[:xyz].present?
        SafinaJob::RecordHandover.perform_later(
          @card_attributes['cardholder_name'],
          @card_attributes['email'].to_sentence,
          @card_attributes['uuid'],
          @card_attributes['card_url']
        )
      end      

      @card_key = encrypt_card_key(
        plain_key: @card_attributes['zipcode'].first + @card_attributes['phone_number'].first
      )
    rescue ArgumentError
      redirect_to '/404' and return
    end
  end

  def retrieve_card_attributes(card:)
    client = AirtableClient.new(extension: ENV['AIRTABLE_SAFINA'])
    api_response = client.send_get_request(endpoint: "PrepaidCard/#{card}")
    
    raise ArgumentError if api_response.status.to_s != '200'
    valid_keys = [
      'Expiry Date', 'Status', 'Card Number', 'UUID', 'Currency', 'Card URL',
      'Current Balance', 'Initial Balance', 'Issued At', 'Last Checked At',
      'Cardholder Name', 'Phone Number', 'Email', 'Zipcode', 'Code',
    ]

    attributes_hash = JSON.parse(api_response.body)['fields'].slice(*valid_keys)
    attributes_hash['Code'] = attributes_hash['Code'].split('-').last
    attributes_hash['UUID'] = attributes_hash['UUID'].split('-').first.upcase

    @card_attributes = attributes_hash.transform_keys{ |key| key.strip.gsub(' ', '_').underscore }
  end

  def setup_simple_crypt
    key = ActiveSupport::KeyGenerator.new('card_password')
                                     .generate_key(ENV['CARD_SALT'])
    ActiveSupport::MessageEncryptor.new(key)
  end

  def encrypt_card_key(plain_key:)
    crypt = setup_simple_crypt

    crypt.encrypt_and_sign(plain_key)  
  end
end
