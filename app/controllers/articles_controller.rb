class ArticlesController < ApplicationController
  before_filter :set_as_static, only: [:index, :show]

  add_breadcrumb 'Oasis', :root_path

  def index
    @posts = Article.all
    @keywords = 'Ilmu muamalah, ekonomi syariah, bisnis Islam'
  end

  def show
    @post = Article.friendly.find(params[:id])
	@keywords = @post.keywords.join(', ')

    add_breadcrumb @post.title, article_path(@post)
  end
end
