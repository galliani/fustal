# This class handles the logic for requests related to auth0
class Auth0Controller < ApplicationController
  skip_before_action :verify_authenticity_token, only: :callback

  def callback
    # This stores all the user information that came from Auth0
    # and the IdP
    session[:userinfo] = request.env['omniauth.auth']
    # Process the auth hash to get the user
    user = Auth0Authentication.new(request.env['omniauth.auth']).run!
    session[:current_user_id] = user.id

    flash.notice = 'Signed in!'
    # Redirect to the URL you want after successfull auth
    redirect_to root_path
  end

  def logout
    session[:userinfo] = nil
    session[:current_user_id] = nil

    url = ENV['FULLHOST'] + '/id/logged_out&client_id=' + ENV['AUTH0_CLIENT_ID']

    flash.notice = 'Log out berhasil!'
    if Rails.env.test?
      redirect_to page_path('logged_out')
    else
      redirect_to "https://kapiten.au.auth0.com/v2/logout?returnTo=#{url}"
    end
  end

  def failure
    # show a failure page or redirect to an error page
    flash[:error] = request.params['message'].to_sentence
  end
end
