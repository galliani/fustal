module Admin
  class SafinaController < BaseController
    def prepare_invoice
    end

    def prepare_reload_payment_notif
    end

    def create_invoice
      params[:merchantCode] = ENV['DUITKU_SAFINA_MERCHANT_CODE']
      params[:paymentMethod] = 'BT'
      params[:callbackUrl] = ENV['DUITKU_SAFINA_CALLBACK_URL']
      params[:returnUrl] = 'sikapiten.com/safina/payment_redirect'
      params[:category] = 'PURCHASE'

      require 'digest/md5'
      combined_input = params[:merchantCode] + params[:merchantOrderId] +
                       params[:paymentAmount] + ENV['DUITKU_SAFINA_KEY']
      signature = Digest::MD5.hexdigest(combined_input)

      params[:signature] = signature

      res = NetRequestInterface.new(ENV['DUITKU_API_URL'] + 'inquiry').send_post_request(params)

      if res.success?
        invoice_data = JSON.parse(res.body)

        client = AirtableClient.new(extension: ENV['AIRTABLE_SAFINA'])
        invoice_fields = {
          :Category => params[:category],
          :"Merchant Order ID" => params[:merchantOrderId],
          :"Invoice Page URL" => invoice_data['paymentUrl'],
          :Signature => signature,
          :Provider => "Duitku",
          :"Provider Invoice ID" => invoice_data['reference'],
          :"Merchant User Info" => params[:merchantUserInfo],
          :Email => params[:merchantUserInfo],
          :Amount => params[:paymentAmount]
        }
        invoice_input = { :fields => invoice_fields }.to_json

        second_res = client.send_post_request(endpoint: 'Invoice', data: invoice_input)

        if second_res.success?
          airtable_invoice_response = JSON.parse(second_res.body)
          transaction_fields = {
            :"Prepaid Card" => [params[:merchantOrderId]],
            :Invoice => [airtable_invoice_response['id']]
          }
          transaction_input = { :fields => transaction_fields }.to_json

          third_res = client.send_post_request(endpoint: 'Transaction', data: transaction_input)

          if third_res.success?
            flash[:notice] = 'Successfully sent request to create transaction record'
          else
            flash[:notice] = 'Successfully sent request to create invoice'
          end
        else
          flash[:alert] = 'Failed request to create invoice'
        end
      else
        flash[:alert] = 'Failed request to create invoice'
      end

      redirect_to admin_safina_prepare_invoice_path
    end

    def send_reload_payment_notif
      if params[:transactionUUID].present?
        query = "maxRecords=1&view=Grid%20view&filterByFormula=({UUID} = #{params[:transactionUUID]})"
        endpoint = 'Transaction?' + query

        client = AirtableClient.new(extension: ENV['AIRTABLE_SAFINA'])
        res = client.send_get_request(endpoint: endpoint)

        invoice_data = JSON.parse(res.body)
        payload = invoice_data['records'][0]['fields']
        humanized_amount = ActionController::Base.helpers.number_to_currency(
          payload['Value'][0], unit: 'Rp ', delimiter: '.', separator: ','
        )

        delivery = SafinaTransactionMailer.card_reload_payment_notif(
          payload['Payer Name'][0], 
          payload['Payer Email'][0], 
          payload['Card Label'][0], 
          humanized_amount, 
          payload['Payment URL'][0]
        )

        if delivery['code'] == 'success'
          flash[:notice] = 'Payment prompt has been delivered'
        else
          flash[:alert] = 'Failed to deliver payment prompt'
        end
      else
        flash[:alert] = 'Invalid input, please try again'
      end

      redirect_to admin_safina_prepare_reload_payment_notif_path
    end
  end
end
