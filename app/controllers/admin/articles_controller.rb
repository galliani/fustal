module Admin
  class ArticlesController < BaseController
    add_breadcrumb "Articles", :admin_articles_path

    def index
      @posts = Article.all
    end

    def sync
      @post = Article.friendly.find(params[:id])

      require 'contentful'

      client = Contentful::Client.new(
        space: ENV['CONTENTFUL_ID'],
        access_token: ENV['CONTENTFUL_KEY'],
        dynamic_entries: :auto
      )

      result = nil

      entry = client.entry(@post.contentful_id)

      if entry.present?
        author = entry.fields[:author].resolve
        header = entry.fields[:header].resolve
        keywords = entry.keywords.map{ |x| x.resolve }.map{ |y| y.fields[:label] }

        datum = {
          contentful_id: entry.id,
          title: entry.title,
          header_name: header.file.file_name,
          header_url: header.image_url,
          category: entry.category,
          teaser: entry.teaser,
          published_at: entry.published_at,
          last_sync_at: DateTime.now,
          body: entry.body,
          keywords: keywords,
          author_id: author.id,
          author_name: author.name,
          author_email: author.email
        }

        result = @post.update(datum)
      end

      if result.nil?
        flash[:notice] = 'Synchronization for the article has failed'
      else
        flash[:notice] = 'Synchronization for the article is successful'
      end
      redirect_to admin_articles_path
    end

    def bulk_sync
      require 'contentful'

      client = Contentful::Client.new(
        space: ENV['CONTENTFUL_ID'],
        access_token: ENV['CONTENTFUL_KEY'],
        dynamic_entries: :auto
      )
      data = []
      counter = 0

      entries = client.entries(content_type: 'blogPost')

      unless entries.nil? && entries.count.zero?

        entries.each do |entry|
          content =  client.entry(entry.id)
          author = content.fields[:author].resolve
          keywords = content.fields[:keywords].map(&:resolve)

          data << {
            contentful_id: entry.id,
            title: entry.title,
            header_name: entry.header.file.file_name,
            header_url: entry.header.image_url,
            category: entry.category,
            teaser: entry.teaser,
            published_at: entry.published_at,
            last_sync_at: DateTime.now,
            body: entry.body,
            keywords: keywords.map(&:label),
            author_id: author.id,
            author_name: author.name,
            author_email: author.email
          }
        end

        puts data

        data.each do |datum|
          result = Article.where(contentful_id: datum[:contentful_id])
                          .update_or_create(datum)

          next unless result

          counter += 1
        end
      end

      flash[:notice] = "Sinkronisasi atas #{counter} artikel telah dilakukan"
      redirect_to admin_articles_path
    end
  end
end
