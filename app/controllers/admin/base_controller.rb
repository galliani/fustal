class Admin::BaseController < ApplicationController
  before_filter :authenticate_user!
  before_filter :require_admin!
  before_action :admin_layout

  add_breadcrumb "Dashboard", :admin_root_path

  def index
  end

  private

  def admin_layout
    @admin_layout = true
  end

end
