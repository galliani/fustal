module CurrentUser
  extend ActiveSupport::Concern
  included do
    helper_method :current_user, :user_signed_in?, :authenticate_user!
  end

  def current_user
    User.find_by(id: session[:current_user_id])
  end

  def user_signed_in?
    session.key?(:current_user_id)
  end

  def current_user?(user)
    return true if current_user == user
    false
  end

  def authenticate_user!
    redirect_to root_path unless session[:current_user_id].present?
  end
end
