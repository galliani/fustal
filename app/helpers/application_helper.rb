module ApplicationHelper
  def body_class(class_name = 'default_class')
    content_for :body_class, class_name
  end

  def admin_controller?
    controller.class.name.split("::").first=="Admin"
  end

  def language_picker
    return 'English' if I18n.locale == :en
    'Bahasa Indonesia'
  end

  def embedded_svg(filename, options = {})
    file = File.read(Rails.root.join('app', 'assets', 'images', filename))
    doc = Nokogiri::HTML::DocumentFragment.parse file
    svg = doc.at_css 'svg'
    svg['class'] = options[:class] if options[:class].present?
    doc.to_html.html_safe
  end

  def guest_user_prompt(purpose, redirect_url = request.path)
    content_tag :div, '', id: 'guest_user_prompt', class: 'centered' do
      content_tag(:p) do
        content_tag(:span, "Untuk #{purpose} silahkan ") +
          content_tag(:span) do
            link_to 'masuk', '/login', class: 'auth0-link auth0-login'
          end +
          content_tag(:span, ' atau ') +
          content_tag(:span) do
            link_to 'registrasi', '/signup', class: 'auth0-link auth0-signup'
          end +
          content_tag(:span, ' terlebih dahulu.')
      end
    end
  end
end
