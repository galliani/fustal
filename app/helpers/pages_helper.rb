module PagesHelper

  def headline_text
    'Belajar berbisnis sesuai ajaran Islam'
  end

  def explainer_text
    "Belajar berbisnis sesuai dengan syariat Islam
    melalui diskusi ilmu muamalah dan penerapannya"
  end


end
