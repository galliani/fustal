module UsersHelper
  def render_user_thumb(options = {})
    AvatarRenderer.new(user_id: options[:user_id], css_class: options[:style]).thumbnail_display
  end

  def render_user_bigger_thumb(options={})
    AvatarRenderer.new(user_id: options[:user_id], css_class: options[:style]).profile_display
  end

  def user_education(user)
    education = if user.last_education.nil? then 'info pendidikan belum diisi' else user.last_education end
    content_tag(:p, education, id: 'user-education')
  end

  def user_occupation(user)
    occupation = if user.occupation.nil? then 'info pekerjaan belum diisi' else user.occupation end
    content_tag(:p, occupation, id: 'user-occupation')
  end

  def user_location(user)
    location = if user.location.nil? then 'info lokasi belum diisi' else "Tinggal di #{user.location}" end
    content_tag(:p, location, id: 'user-location')
  end

  def user_bio(user)
    bio = if user.about.nil? then 'bio belum diisi' else user.about end
    content_tag(:p, bio, id: 'user-bio')
  end

  def poster_identifier(post)
    if post.confidentiality?
      post.user_name
    else
      link_to post.user_name, user_path(post.user)
    end
  end
end
