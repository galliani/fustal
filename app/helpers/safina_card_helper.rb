module SafinaCardHelper
  def humanized_last_checked_at(string)
    string.to_time.strftime("%d/%m/%y %H:%M")
  end

  def show_card_balance(card)
    card.current_balance + "(#{card.initial_balance}) " + card.currency
  end

  def show_card_status(status)
    return 'Belum aktif' if status == 0
    return 'Aktif' if status == 1
    return 'Dimatikan' if status == 2
  end

  def humanized_issued_at(string)
    string.to_date.strftime("%d/%m/%y")
  end
end
