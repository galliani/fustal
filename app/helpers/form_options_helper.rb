module FormOptionsHelper
  def boolean_options
  	[ ["True", true], ["False", false] ]
  end

  def yes_no_options
  	[ "yes", "no" ]
  end

  def currency_options
  	[ "idr", "usd" ]
  end


  def last_education_options
	  [ "Di bawah SMP", "SMA", "D3", "S1", "S2", "Doktoral dan seterusnya" ]
  end

  def omniauth_button_display(provider, login=false)
  	if login == true
  	  "Login dengan #{provider}"
  	else
  	  "Mendaftar dengan #{provider}"
  	end
  end

  def auth_class_button(login=false)
    if login == true
      "login-btn"
    else
      "signup-btn"
    end
  end

end