# require 'contentful'

# module Contentful
#   class BulkSync < ActiveInteraction::Base
#     array :entries
#     array :data, default: []
#     integer :counter, default: 0

#     def execute
#       entries = client.entries(content_type: 'blogPost')

#       unless entries.nil? && entries.count.zero?
#         entries.each do |entry|
#           next if entry.draft == true

#           content =  client.entry(entry.id)
#           author = content.fields[:author].resolve

#           data << build_hash(entry: entry, author: author)
#         end

#         data.each do |datum|
#           result = create_or_update_article(datum[:contentful_id])

#           next unless result

#           counter += 1
#         end
#       end

#       return counter
#     end

#     private

#     def client
#       Contentful::Client.new(
#         space: ENV['CONTENTFUL_ID'],
#         access_token: ENV['CONTENTFUL_KEY'],
#         dynamic_entries: :auto
#       )
#     end

#     def build_hash(entry:, author:)
#       {
#         contentful_id: entry.id,
#         title: entry.title,
#         header_name: entry.header.file.file_name,
#         header_url: entry.header.image_url,
#         category: entry.category,
#         teaser: entry.teaser,
#         published_at: entry.published_at,
#         last_sync_at: DateTime.now,
#         body: entry.body,
#         author_id: author.id,
#         author_name: author.name,
#         author_email: author.email
#       }
#     end

#     def create_or_update_article(contentful_id)
#       Article.where(contentful_id: contentful_id).update_or_create(datum)
#     end
#   end
# end
