class Auth0Authentication
  def initialize(auth)
    @auth = auth
    @info = auth[:info]
    @raw = auth[:extra][:raw_info]
    @metadata = @raw[:user_metadata] if @raw[:user_metadata]
    @identities = @raw[:identities] if @raw[:identities]
    @provider = @identities.first[:provider]
    if @metadata
      @name = @metadata[:name]
    else
      @name = @raw[:name]
    end
  end

  def run!
    # try to find exisiting user
    email = @info[:email]|| @raw[:email]
    user = User.find_by(email: email)

    if user.nil?
      return nil
    else
      # Update missing user attributes or refresh it
      refresh_user_attributes(user: user, email: email)
      # Notify via slack of the login
      login_callback(user: user)
    end

    # return the existing user OR the newly created user
    user
  end

  private

  def create_user(email:)
    User.create(email: email) do |user|
      user.password   = Devise.friendly_token[0,20]
      user.password_confirmation = user.password
      user.name       = @name
      user.first_name = @raw[:first_name] if @raw[:first_name]
      user.last_name  = @raw[:last_name] if @raw[:last_name]
      user.avatar     = @raw[:picture] if @raw[:picture]
      user.auth_with  = @provider
      user.avatar_source = 'auth0'
      user.understanding = 'yes'
      user.current_sign_in_ip = @raw[:last_ip] if @raw[:last_ip]
    end
  end

  def refresh_user_attributes(user:, email:)
    user.email ||= email
    user.name = @name
    user.first_name  ||= @raw[:first_name]
    user.last_name   ||= @raw[:last_name]
    user.auth_with = @provider
    # FIXME: remove update for avatar and avatar_source once all
    # users have been migrated
    user.avatar = @raw[:picture]
    user.avatar_source = 'auth0'
    user.current_sign_in_ip = @raw[:last_ip] if @raw[:last_ip]
    user.last_sign_in_ip = user.current_sign_in_ip
    # Update the user!
    user.increment(:sign_in_count)
    user.save
  end

  def registration_callback(user:)
    if live_or_staging?
      SlackNotifierJob.perform_later(user.registration_message)
      UserMailerJob::SendWelcome.perform_later(user.email, user.name)
    end
  end

  def login_callback(user:)
    SlackNotifierJob.perform_later(user.login_message) if live_or_staging?
  end

  def live_or_staging?
    Rails.env.production? || Rails.env.staging?
  end
end
