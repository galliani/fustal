require 'slack-notifier'

class SlackNotifier
  include ActiveModel::Model

  attr_accessor :message  
  validates :message, presence: true

  def initialize(message)
  	@message = message
    @client = Slack::Notifier.new(ENV["SLACK_WEBHOOK_URL"], 
        username: ENV["SLACK_NOTIF_USERNAME"])
  end

  def send_ping
    @client.ping(@message)
  end

end