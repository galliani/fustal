class Article < ActiveRecord::Base
  self.table_name = "articles"

  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  serialize      :header_details, HashSerializer
  store_accessor :header_details,
                 :header_name, :header_url

  serialize      :author_details, HashSerializer
  store_accessor :author_details,
                 :author_id, :author_email, :author_name

  def self.update_or_create(attributes)
    assign_or_new(attributes).save
  end

  def self.assign_or_new(attributes)
    obj = first || new
    obj.assign_attributes(attributes)
    obj
  end

  private

  def slug_candidates
    [
      :title
    ]
  end
end

