class SafinaCard
  include ActiveModel::Model

  attr_accessor :uuid, :card_url,
                :cardholder_name, :card_number, :expiry_date, 
                :masked_cardholder_name, :masked_card_number, :masked_expiry_date,
                :code,
                :initial_balance, :current_balance, :currency,
                :zipcode, :phone_number, :email,
                :status, :issued_at, :last_checked_at
end
