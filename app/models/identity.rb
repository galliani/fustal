class Identity < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :uid, :provider
  validates_uniqueness_of :uid, :scope => :provider

  scope :as_facebook_user, -> { where(provider: 'facebook').first }
  scope :as_google_user, -> { where(provider: 'google_oauth2').first }

  def self.create_with_omniauth(auth)
    raw = auth['extra']['raw_info']
    Identity.create do |identity|
      identity.provider     = auth.provider
      identity.uid          = auth.uid
      identity.token        = auth.credentials.token
      identity.secret       = auth.credentials.secret if auth.credentials.secret
      identity.expires_at   = auth.credentials.expires_at if auth.credentials.expires_at
      identity.email        = auth.info.email if auth.info.email
      identity.location     = raw['location']['name'] if raw['location']
      if raw['first_name'] || auth.info.first_name
        identity.first_name = raw['first_name'] || auth.info.first_name
      end
      if raw['last_name'] || auth.info.last_name
        identity.last_name = raw['last_name'] || auth.info.last_name
      end
      if raw['profile'] || raw.link
        identity.public_url = raw['profile'] || raw.link
      end
      unless identity.public_url.blank?
        identity.image = auth.info.image if auth.info.image
      end
    end
  end

  def update_user_missing_info
    user.email ||= email
    user.first_name  ||= first_name
    user.last_name   ||= last_name
    user.name = "#{first_name} #{last_name}"
    user.location ||= location
    user.understanding = 'yes'
    user.auth_with = provider
    user.avatar ||= image
    if user.avatar_was.blank? && user.avatar.present?
      user.avatar_source = provider
    end
    user.skip_reconfirmation!
    user.save!
  end

end
