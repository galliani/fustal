class User < ActiveRecord::Base
  include UserAdmin

  serialize :preferences, HashSerializer
  store_accessor :preferences,
                 :currency, :open, :understanding, :notification
  serialize :profile, HashSerializer
  store_accessor :profile,
                 :phone_number, :about, :last_education, :browser_id, :address,
                 :location, :marital_status, :work_experience, :occupation,
                 :number_dependents, :monthly_income, :monthly_expense

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :validatable

  # Pagination
  paginates_per 30

  # Validations
  validates :name, presence: true
  validates :email, presence: true

  before_create :set_preferences!

  scope :omniauthers, lambda {
    where.not('profile @> ?', { auth_with: 'kapiten' }.to_json)
  }
  scope :admins, -> { where(admin: true) }

  def anonym_mask(post_ip:)
    'Anonim dengan IP address ' + post_ip.to_s
  end

  def registration_message
    "A user with email #{email} just registered"
  end

  def login_message
    "#{email} has just logged in"
  end

  private

  def set_preferences!
    self.currency = 'idr'
    self.open = 'yes'
    self.notification = 'yes'
  end
end
