require 'sendinblue'

class UserMailer

  def self.say_welcome(email, name)
    data = {
      "id" => 1, #template id
      "to" => email.to_s,
      "attr" => { "FULLNAME" => name.to_s }
    }
    client = Sendinblue::Mailin.new("https://api.sendinblue.com/v2.0", "#{ENV['SENDINBLUE_API_KEY']}")
    result = client.send_transactional_template(data)
    puts result
  end

  def self.notify_reply(email, name, postername, topicname, topicurl)
    data = {
      "id" => 2, #template id
      "to" => email.to_s,
      "attr" => {
        "FULLNAME" => name.to_s,
        "POSTERNAME" => postername.to_s,
        "TOPICNAME" => topicname.to_s,
        "TOPICURL" => topicurl
      }
    }
    client = Sendinblue::Mailin.new("https://api.sendinblue.com/v2.0", "#{ENV['SENDINBLUE_API_KEY']}")
    result = client.send_transactional_template(data)
    puts result
  end

end
