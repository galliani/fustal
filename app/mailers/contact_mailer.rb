require 'sendinblue'

class ContactMailer < ActionMailer::Base
  add_template_helper(EmailHelper)
  default from: ENV["ZOHO_USERNAME"]


  def contact_message(name, email, subject, message)
    data = {
      "id" => 6, #template id
      "to" => ENV['SENDER_EMAIL'],
      "attr" => {
        "FULLNAME" => name.to_s.titleize,
        "EMAIL" => email.to_s,
        "SUBJECT" => subject.to_s,
        "MESSAGE" => message.to_s
      }
    }

    client = Sendinblue::Mailin.new(
      'https://api.sendinblue.com/v2.0', ENV['SENDINBLUE_API_KEY']
    )
    result = client.send_transactional_template(data)
    puts result
  end
end