require 'sendinblue'

class SafinaTransactionMailer
  def self.card_reload_payment_notif(name, email, card_identifier, humanized_balance, payment_url)
    data = {
      "id" => 8, #template id
      "to" => email,
      "from" => ENV["ZOHO_USERNAME"],
      "attr" => {
        "FULLNAME" => name.to_s.titleize,
        "CARDID" => card_identifier,
        "AMOUNT" => humanized_balance,
        "PAYMENTURL" => payment_url,      }
    }

    client = Sendinblue::Mailin.new(
      'https://api.sendinblue.com/v2.0', ENV['SENDINBLUE_API_KEY']
    )

    client.send_transactional_template(data)
  end
  
  # def order_summary(name, email, humanized_balance)
  #   data = {
  #     "id" => 2, #template id
  #     "to" => email,
  #     "attr" => {
  #       "FULLNAME" => name.to_s.titleize,
  #       "BALANCE" => humanized_balance
  #     }
  #   }

  #   client = Sendinblue::Mailin.new(
  #     'https://api.sendinblue.com/v2.0', ENV['SENDINBLUE_API_KEY']
  #   )

  #   result = client.send_transactional_template(data)
  #   puts result
  # end

  # def order_confirmation(name, email, humanized_balance, payment_url)
  #   data = {
  #     "id" => 3, #template id
  #     "to" => email,
  #     "attr" => {
  #       "FULLNAME" => name.to_s.titleize,
  #       "PAYMENTURL" => payment_url,
  #       "BALANCE" => humanized_balance
  #     }
  #   }

  #   client = Sendinblue::Mailin.new(
  #     'https://api.sendinblue.com/v2.0', ENV['SENDINBLUE_API_KEY']
  #   )

  #   result = client.send_transactional_template(data)
  #   puts result
  # end

  # def order_completed(name, email, card_identifier, card_url)
  #   data = {
  #     "id" => 4, #template id
  #     "to" => email,
  #     "attr" => {
  #       "FULLNAME" => name.to_s.titleize,
  #       "PAYMENTURL" => card_url,
  #       "CARDID" => card_identifier
  #     }
  #   }

  #   client = Sendinblue::Mailin.new(
  #     'https://api.sendinblue.com/v2.0', ENV['SENDINBLUE_API_KEY']
  #   )

  #   result = client.send_transactional_template(data)
  #   puts result
  # end
end
