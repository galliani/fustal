module UserMailerJob
  class SendWelcome < ActiveJob::Base
    queue_as :default

    def perform(email, name)
      UserMailer.say_welcome(email, name)
    end
  end

  class NotifyReply < ActiveJob::Base
    queue_as :default

    def perform(email, name, postername, topicname, topicurl)
      UserMailer.notify_reply(email, name, postername, topicname, topicurl)
    end
  end
end
