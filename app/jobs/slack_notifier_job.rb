class SlackNotifierJob < ActiveJob::Base
  queue_as :default

  def perform(message)
    SlackNotifier.new(message).send_ping
  end
end
