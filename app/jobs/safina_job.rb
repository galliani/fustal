module SafinaJob
  class RecordHandover < ActiveJob::Base
    queue_as :default

    def perform(name, email, uuid, card_url)
      data = {
        "fields" => {
          "UUID" => uuid, "Name" => name, "Email" => email, "URL" => card_url
        }
      }.to_json

      client = AirtableClient.new(extension: ENV['AIRTABLE_SAFINA'])
      response = client.send_post_request(endpoint: 'PurchaseHandover', data: data)
    end
  end
end
