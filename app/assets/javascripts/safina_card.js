// Card display


$('#view-card-page').ready(
  function SafinaCards() {
    var page = this;

    var DOMstrings = {
      cardContainer: ".safina-view-card-wrapper",
      maskedCardContainer: ".safina-masked-card-wrapper",
      cardElement: ".jp-card-container",
      innerCardElement: ".jp-card",
      innerCardSkin: ".jp-card-shiny",
      passwordField: '#password-input',
      cardKeyField: '#cardKey',
      cardUID: '#cardUID',
      openCardForm: '#card-lock-form',
      reloadForm: '#reload-form',
      closeReloadForm: '#close-reload-card-form',
      submitReloadBtn: '#reload-card-btn',
      unauthenticatedCard: '#unauthenticated-card',
      authenticatedCard: '#authenticated-card',
      csrfMetaTag: 'meta[name=csrf-token]',
      loadingOverlay: '.loading'
    };

    var cardUI = {

      displayMaskedCard: function() {

        return new Card({
          form: 'form', // *required*
          container: DOMstrings.maskedCardContainer, // *required*

          messages: {
              validDate: 'valid\ndate', // optional - default 'valid\nthru'
              monthYear: 'mm/yyyy', // optional - default 'month/year'
          },

          placeholders: {
              number: gon.maskedCardNumber,
              name: gon.maskedCardholderName,
              expiry: gon.maskedExpiryDate,
              cvc: '•••'
          }
        });

      },

      displayPlainCard: function() {

        return new Card({
          form: 'form', // *required*
          container: DOMstrings.cardContainer, // *required*

          messages: {
              validDate: 'valid\ndate', // optional - default 'valid\nthru'
              monthYear: 'mm/yyyy', // optional - default 'month/year'
          },

          placeholders: {
              number: gon.cardNumber,
              name: gon.cardholderName,
              expiry: gon.expiryDate,
              cvc: '•••'
          }
        }); 

      },

      stylingCard: function() {
        document.querySelector(DOMstrings.cardElement).classList.add( "height-adjusted" );
        document.querySelector(DOMstrings.innerCardElement).classList.add( "jp-card-visa","jp-card-identified" );
        document.querySelector(DOMstrings.innerCardSkin).classList.add( "margin-15p-btm" );      
      },

      getLockInput: function() {
        // Just return JSON object
        return {
          cardUID: document.querySelector(DOMstrings.cardUID).value,
          password: document.querySelector(DOMstrings.passwordField).value,
          cardKey: document.querySelector(DOMstrings.cardKeyField).value
        };
      },

      toggleUnauthenticatedDisplay: function(block_or_none) {
        document.querySelector(DOMstrings.unauthenticatedCard).style.display = block_or_none;
        document.querySelector(DOMstrings.maskedCardContainer).style.display = block_or_none;
      },

      toggleAuthenticatedDisplay: function(block_or_none) {
        document.querySelector(DOMstrings.authenticatedCard).style.display = block_or_none;
        document.querySelector(DOMstrings.cardContainer).style.display = block_or_none;
      },

      toggleReloadFormDisplay: function(block_or_none) {
        document.querySelector(DOMstrings.reloadForm).style.display = block_or_none;
      },

      toggleLoadingSpinner: function(block_or_none) {
        document.querySelector(DOMstrings.loadingOverlay).style.display = block_or_none;
      },

      setMaskCardCountdown: function() {
        setTimeout(function() {
          cardUI.toggleAuthenticatedDisplay('none');
          cardUI.toggleUnauthenticatedDisplay('block');

          var passwordInput = document.querySelector(DOMstrings.passwordField); 
          passwordInput.value = '';
          passwordInput.focus();
        }, 30000);
      }
    }


    var cardController = {
      authenticating: function(cardUID, password, key, csrfToken) {
        // 1. create a new XMLHttpRequest object -- an object like any other!
        var conn = new XMLHttpRequest();
        // 2. open the request and pass the HTTP method name and the resource as parameters
        conn.open('POST', '/safina/d/open');

        conn.setRequestHeader("Content-Type", "application/json");
        conn.setRequestHeader('X-CSRF-Token', csrfToken)

        conn.send(
          JSON.stringify({ card: cardUID, password: password, cardKey: key})
        );

        return conn;
      }
    }


    var mainController = (function(cardUI, cardCtrl) {
      var setupEventListeners = function() {
        // Open card form
        document.querySelector(DOMstrings.openCardForm).addEventListener('submit', function(event) {
          event.preventDefault();

          // Retrieve the input from the UI
          inputs = cardUI.getLockInput();
          csrfToken = document.querySelector(DOMstrings.csrfMetaTag).content;

          // Make AJAX request to authenticate the card
          req = cardCtrl.authenticating(inputs.cardUID, inputs.password, inputs.cardKey, csrfToken);
          cardUI.toggleLoadingSpinner('block');

          // If authentication is successful, unmask the card
          req.onreadystatechange = function () { 

              // 4. check if the request has a readyState of 4, which indicates the server has responded (complete)
              if (req.readyState === 4) {
                // 5. insert what should happen
                cardUI.toggleLoadingSpinner('none');

                if (req.status === 200) {
                  cardUI.displayPlainCard();
                  cardUI.toggleUnauthenticatedDisplay('none');
                  cardUI.toggleAuthenticatedDisplay('block');
                  cardUI.stylingCard();

                  alertify.success('Berhasil membuka kartu');

                  cardUI.setMaskCardCountdown();
                } else {
                   alertify.error('Gagal membuka kartu, mohon coba lagi');
                }
                
              }
          };
        });

        // Open reload form
        document.querySelector(DOMstrings.submitReloadBtn).addEventListener('click', function() {
          cardUI.toggleAuthenticatedDisplay('none');
          cardUI.toggleReloadFormDisplay('block');
        });

        // Close reload form
        document.querySelector(DOMstrings.closeReloadForm).addEventListener('click', function() {
          cardUI.toggleReloadFormDisplay('none');
          cardUI.toggleAuthenticatedDisplay('block');
        });

        // Submission for reload-card-form 
        document.querySelector(DOMstrings.reloadForm).addEventListener('submit', function() {
          document.querySelector('.loading').style.display = 'block';
        });
      }

      return {
        init: function() {
          cardUI.displayMaskedCard();
          cardUI.stylingCard();
          setupEventListeners();
        }
      };
    })(cardUI, cardController);

    mainController.init();
  }
)
