// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require jquery.turbolinks
//= require jquery.countTo
//= require jquery.easing.1.3
//= require jquery.magnific-popup.min
//= require jquery.waypoints.min
//= require jquery.card
//= require magnific-popup-options
//= require modernizr-2.6.2.min
//= require respond.min
//= require main
//= require bootstrap
//= require carousel
//= require alertify

$(document).on("ready page:change", function()  {
  // Card sample display
  $('form.safina-sample-card').card({
  container: '.safina-landing-view-card-wrapper', // *required*
  placeholders: {
    number: '4665 4402 2313 4964',
    name: 'Leo Messi',
    expiry: '02/20',
    cvc: '385'
   }
  });
  $( "div.jp-card" ).addClass( "jp-card-visa jp-card-identified" );
});

// Carousel
$(document).ready(function () {
  $('#myCarousel').carousel({
      interval: 4000,
      cycle: true
  })
  $('.fdi-Carousel .item').each(function () {
      var next = $(this).next();
      if (!next.length) {
          next = $(this).siblings(':first');
      }
      next.children(':first-child').clone().appendTo($(this));

      if (next.next().length > 0) {
          next.next().children(':first-child').clone().appendTo($(this));
      }
      else {
          $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
      }
  });
});
