source 'https://rubygems.org'
ruby "2.3.1"

# Standard Rails gems
gem 'rails', '~> 4.2.0'
gem 'sass-rails', '5.0.2'
gem 'uglifier', '2.6.0'
gem 'coffee-rails', '4.1.0'
gem 'coffee-script-source', '1.8.0'
gem 'jquery-rails', '4.0.3'
gem 'turbolinks', '2.5.3'
gem 'jquery-turbolinks', '~> 2.1.0'
gem 'alertifyjs-rails', '1.11.0'
gem 'json', github: 'flori/json', branch: 'v1.8'

# Authentication
gem 'devise', '3.4.1'
gem 'omniauth', '~> 1.3.1'
gem 'omniauth-oauth2', '~> 1.3.1'
gem 'omniauth-auth0', '~> 1.4.2'

gem 'rollbar', '2.13.3'

# File Upload
# gem 'cloudinary', '~> 1.1.0'
gem "font-awesome-rails", '4.7.0.1'

# Forum
gem 'contentful', '1.1.0'

# Necessities
gem 'tzinfo-data', platforms: [:mingw, :mswin] # Necessary for Windows OS (won't install on *nix systems)
gem 'kaminari', '0.16.2'
gem 'friendly_id', '5.1.0'
gem 'bootstrap-sass', '3.3.3'
gem 'simple_form', '3.2.1'
gem 'pg', '~> 0.18.4'
gem 'meta-tags', '2.3.1', require: 'meta_tags'
gem 'browser', '~> 2.2.0'
gem 'figaro', '1.0.0'
gem 'sucker_punch', '~> 2.0'
gem 'sendinblue', '~> 2.4'
gem 'high_voltage', '2.4.0'
gem 'faraday', '0.9.2'
gem 'faraday_middleware', '0.10.1'
gem 'gon'

# Complementary
gem 'rails-i18n', '~> 4.0.0' # For 4.0.x
gem 'active_link_to', '1.0.3'
gem 'breadcrumbs_on_rails', '~> 2.3.1'
gem 'slack-notifier', '~> 1.5.1'
gem 'redcarpet', '3.2.2'

group :development do
  gem "awesome_print", require:"ap"
  gem "better_errors", '2.1.1'
  gem "binding_of_caller", '0.7.2'
  gem "quiet_assets", '1.1.0'
  gem 'web-console', '2.0.0'
  # gem "bullet", '5.0.0' #not yet implemented
end

group :development, :test do
  gem 'byebug'
  gem 'spring-commands-rspec' # gem 'spring'
  gem 'rspec-rails', '3.3.0' # '3.4.2'
  gem 'factory_girl_rails', '4.5.0'
  gem 'faker', '>= 1.6.2'
  gem 'rack_session_access', '~> 0.1.1'
  gem 'certified'
end

group :test do
  gem 'thin', '1.5.1'
  gem 'capybara', '~> 2.2.0'
  gem 'database_cleaner', '1.3.0'
  gem 'shoulda-matchers', '3.1.0'
  gem 'test_after_commit'
  gem 'webmock', '~> 2.1.0'
end

group :production, :staging do
  gem 'rails_12factor'
  gem 'puma'
end

# Run this command to install gem not in vendor bundle folder
# bundle install --path /home/vagrant/.bundles/fustal --without staging production
