require 'net/http'
require 'uri'

class NetRequestInterface
  def initialize(fullpath)
    @fullpath = fullpath
  end

  def send_get_request
    connection = Faraday.new(url: @fullpath) do |faraday|
      faraday.response :logger                  # log requests to STDOUT
      faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
    end

    connection.get
  end

  def send_post_request(data, content_type = 'application/json')
    connection = Faraday.new(url: @fullpath) do |faraday|
      faraday.response :logger                  # log requests to STDOUT
      faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
    end

    if content_type == 'application/json'
      response = connection.post do |req|
        req.headers['Content-Type'] = 'application/json'
        req.body = data.to_json
      end
    end

    response
  end
end
