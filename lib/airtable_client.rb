require 'faraday'

class AirtableClient
  def initialize(api_key: ENV['AIRTABLE_API_KEY'], extension:)
    @base_url = ENV['AIRTABLE_API_URL'] + extension
    @api_key = api_key
  end

  def send_get_request(endpoint:)
    setup_connection

    @connection.get endpoint
  end

  def send_post_request(endpoint:, data:)
    setup_connection

    @connection.post do |req|
      req.url endpoint
      req.headers['Content-Type'] = 'application/json'
      req.body = data
    end
  end

  private

  def setup_connection
    # start setting up connections
    @connection = Faraday.new(url: @base_url) do |faraday|
      faraday.response :logger                  # log requests to STDOUT
      faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
    end

    # finish setting up connection
    @connection.authorization(:Bearer, @api_key)
  end
end
