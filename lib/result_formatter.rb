class ResultFormatter
  def initialize(success:, output: nil, messages: nil)
    @success = success
    @output = output
    @messages = messages
  end

  # We can use attr_reader for these 3 methods below, but the advantage
  # of declaring manually like this is easy to customize per requirements
  def success
    @success
  end

  def output
    @output
  end

  def messages
    @messages
  end
end
