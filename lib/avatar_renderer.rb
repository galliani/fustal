require 'cloudinary'

class AvatarRenderer
  include CloudinaryHelper

  def initialize(user_id: nil, css_class: nil)
    @user_id = user_id
    @css_class = css_class
  end

  Anonymous = Struct.new(:avatar, :avatar_source)

  def user
    return Anonymous.new('', 'kapiten') if @user_id.nil?
    User.find_by(id: @user_id)
  end

  def thumbnail_display(source: avatar_provider)
    url = formulate_url(source: source, size: 'thumb')
    image_tag(url, class: @css_class)
  end

  def profile_display(source: avatar_provider)
    url = formulate_url(source: source, size: 'mid')
    image_tag(url, class: @css_class)
  end

  def avatar_url(size, source: avatar_provider)
    formulate_url(source: source, size: size)
  end

  private

  def formulate_url(source:, size: nil)
    if source == 'auth0'
      url = user.avatar
    else
      url = cloudinary_sourcing(source, size)
    end
  end

  def avatar_provider
    return 'auth0' if user.avatar_source.blank?
    user.avatar_source
  end

  def cloudinary_sourcing(source, size)
    options = sizing(size)

    h = plain_sourcing

    cl_image_path(h[:public_id], options.merge(h[:defaults]))
  end

  def plain_sourcing
    if user.avatar.blank?
      public_id = default_avatar
      defaults = { type: :upload, format: :jpg }
    else
      public_id = user.avatar
      defaults = { type: :private }
    end
    { public_id: public_id, defaults: defaults }
  end

  def sizing(size)
    if size == 'thumb'
      { width: 100, height: 100, quality: 80, crop: :scale }
    elsif size == 'mid'
      { width: 175, height: 175, quality: 80, crop: :scale }
    elsif size == 'lg'
      { width: 350, height: 350, quality: 100, crop: :scale }
    end
  end

  def default_avatar
    'avatar/default_avatar'
  end
end
