Fustal::Application.routes.draw do
  # Publication
  resources :articles, as: 'articles', path: App::Naming::BLOG.downcase, only: [:show, :index]

  post '/send_contact_email', to: 'application#send_contact_email', as: :send_contact_email

  # Safina
  get '/safina/payment_redirect', to: 'safina#redirector', id: 'redirector'
  get '/safina', to: 'safina#landing', as: 'safina_root'
  get '/safina/help', to: 'safina#help', as: 'safina_knowledge_base'
  get '/safina/aqad', to: 'safina#sharia_compliance', as: 'safina_sharia_compliance'
  get '/safina/contact', to: 'safina#contact', as: 'safina_contact_page'
  get '/safina/order', to: 'safina#order_form', as: 'safina_order_form'
  get '/safina/d', to: 'safina#view_card', as: 'safina_view_card'
  post '/safina/d/open', to: 'safina#open_card', as: 'safina_open_card'
  post '/safina/d/reload', to: 'safina#request_reload_card', as: 'safina_request_reload_card'

  # Auth0
  get '/auth/auth0/callback' => 'auth0#callback'
  delete '/auth/auth0/logout' => 'auth0#logout'
  get '/auth/failure' => 'auth0#failure'

  # New App
  resources :users, only: [:show, :edit, :update] do
    member do
      get 'avatar'
      put 'remove_avatar'
    end
  end

  # Static Pages
  get '/failed_payment', to: 'pages#show', id: 'failed_payment'
  get '/id/*id' => 'pages#show', as: :page, format: false
  
  # Error pages
  get '/404' => 'errors#not_found'
  get '/422' => 'errors#unprocessable'
  get '/500' => 'errors#internal_server_error'

  namespace :admin do
    root 'base#index'
    resources :users
    resources :articles, only: [:index] do
      collection { post 'bulk_sync' }
      member { post 'sync' }
    end

    # SAFINA Admin
    get 'safina/prepare_invoice', to: 'safina#prepare_invoice'
    post 'safina/create_invoice', to: 'safina#create_invoice'

    get 'safina/prepare_reload_payment_notif', to: 'safina#prepare_reload_payment_notif'
    post 'safina/send_reload_payment_notif', to: 'safina#send_reload_payment_notif'
  end

  # Robot script
  get '/robots.txt' => RobotsTxt

  # Root
  root to: 'articles#index'
end
