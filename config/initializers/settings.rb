module App
  class Description
  	CARD = 'Safina adalah produk dari Kapiten yang membantu kamu memperoleh kartu debit virtual Visa untuk belanja online atau membayar tagihan di situs-situs internasional yang menerima kartu VISA sebagai alat pembayaran'
  	BLOG = 'Pustaka adalah media publikasi yang bertujuan untuk membantu
  kamu mempelajari ilmu muamalah dan praktiknya di dalam kehidupan sehari-hari'
  end

  class Naming
	  BLOG = 'Pustaka'
	  TICKETING = 'Safina'
    CARD = 'Safina'
  end

  class Source
  	ICON = 'http://res.cloudinary.com/instilla/image/upload/s--LjyhjOx8--/c_crop,h_250,w_320/v1504013463/asset/transparent-logo-only.png'
  	BLOG_IMAGE = 'https://res.cloudinary.com/instilla/image/upload/s--VjMLNgTK--/c_fill,w_1300/v1480847243/asset/glyphsco-1459080062.jpg'
  	SAFAR_IMAGE = 'https://res.cloudinary.com/instilla/image/upload/c_scale,w_1400/v1475459177/asset/'
    CARD_IMAGE = 'https://res.cloudinary.com/instilla/image/upload/s--BCheE8iZ--/v1492695792/asset/png/Safina_Promo_Image.png'
  end

  class Title
  	CARD = 'Safina | Kartu Debit Visa Untuk Belanja Online di Situs Internasional '
  end
end
