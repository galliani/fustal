Rails.application.config.assets.precompile += %w( safina.scss )
Rails.application.config.assets.precompile += %w( safina.js )
Rails.application.config.assets.precompile += %w( safina_card.css )
Rails.application.config.assets.precompile += %w( safina_card.js )
