Rails.configuration.middleware.use Browser::Middleware do
  agent_strings = /Opera Mini|Opera Mobi|UBrowser|UCBrowser/

  # unless browser.modern? || request.env['HTTP_USER_AGENT'] =~ agent_strings
  if request.env['HTTP_USER_AGENT'] =~ agent_strings
  	redirect_to page_path('upgrade')
  end
end
